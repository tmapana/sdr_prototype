//
// Code adapted from Ettus Research: https://github.com/EttusResearch/uhd/
//

#include <uhd/types/tune_request.hpp>
#include <uhd/utils/thread_priority.hpp>
#include <uhd/utils/safe_main.hpp>
#include <uhd/utils/static.hpp>
#include <uhd/usrp/multi_usrp.hpp>
#include <uhd/exception.hpp>
#include <boost/program_options.hpp>
#include <boost/format.hpp>
#include <boost/thread.hpp>
#include <boost/math/special_functions/round.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/filesystem.hpp>
#include <iostream>
#include <fstream>
#include <complex>
#include <csignal>

namespace po = boost::program_options;

///*****************************************//
///             signal handlers             //
///*****************************************//
static bool stop_signal_called = false;
void sig_int_handler(int) { stop_signal_called = true; }

///*****************************************//
///           transmit_from_file            //
///*****************************************//
size_t transmit_from_file (
    uhd::usrp::multi_usrp::sptr usrp,
    uhd::tx_streamer::sptr& tx_stream,
    size_t samples_per_buffer,
    std::string& tx_file,
    double total_time,
    double pri,
    double pulse_width,
    size_t total_num_pulses
) {
    // setup the metadata flags
    uhd::tx_metadata_t tx_md;
    tx_md.start_of_burst = true;
    tx_md.end_of_burst = false;
    tx_md.has_time_spec = true;
    tx_md.time_spec = uhd::time_spec_t(0.1); //give us 0.1 seconds to fill the tx buffers

    std::ifstream in_file(tx_file.c_str(), std::ifstream::binary);  // open file

    // read entire file into buffer a buffer
    std::vector<std::complex<short> > tx_buffs(samples_per_buffer);
    std::cout << "Allocated transmit buffer with " << samples_per_buffer << " complex short samples" << std::endl;

    // the first call to send() will block this many seconds before sending:
    double timeout = 1.5; //timeout (delay before transmit + padding)

    // get start time
    boost::system_time start = boost::get_system_time();
    boost::system_time last_update = start;
    boost::posix_time::time_duration diff;

    // send requested samples of data
    size_t pulses = 0;
    double time_duration = 0.0;
    size_t total_num_samps = 0;
    while (not tx_md.end_of_burst and not stop_signal_called) {
        boost::system_time now = boost::get_system_time(); // get current system time

        in_file.read((char*)&tx_buffs.front(), 2 * tx_buffs.size() * sizeof(short));	//read a complex values [i,j]

        size_t num_tx_samps = size_t(in_file.gcount()/sizeof(short));

        if ((time_duration > (double)total_time) and (total_time > 0))	break;	//transmit duration has been exceeded

        else if ((pulses > total_num_pulses) and (pri > 0))	break;	//all requested pulses have been transmitted

        else if (((double)total_time != 0) and (time_duration <= (double)total_time)) {
            // sending requested samples
            tx_stream->send(
                &tx_buffs.front(), num_tx_samps, tx_md, timeout
            );

            diff = now - last_update;
            time_duration += (double)diff.ticks() / (double)boost::posix_time::time_duration::ticks_per_second();

            last_update = now;

            std::cout << "Transmission duration: " << time_duration << " [s]" << std::endl;
            //total_num_samps += num_tx_samps;
        }

        else if (pulses < total_num_pulses) {
            tx_stream->send(
                &tx_buffs.front(), num_tx_samps, tx_md, pri
            );	//send short pulse of duration pulse_width

            //double dead_time = pri - pulse_width;
            //boost::this_thread::sleep(boost::posix_time::seconds(dead_time));

            pulses++;
            std::cout << "Pulse " << pulses << " transmitted" << std::endl;
        }

        else {
            // sending requested samples
            tx_stream->send(&tx_buffs.front(), num_tx_samps, tx_md, timeout);

            total_num_samps += num_tx_samps;
            //std::cout << "Samples sent: " << total_num_samps << std::endl;

            // checking if the end of the file has been reached
            tx_md.end_of_burst = in_file.eof();
        }

        timeout = 0.1;

    }

    in_file.close();    // close file and cleanup

    // send a mini EOB packet
    tx_md.end_of_burst = true;
    tx_stream->send("", 0, tx_md);

    // reset usrp time to prepare for receive
    std::cout << "Finished transmitting!" << std::endl;

    if ((double)total_time != 0)
        std::cout << "Total transmission time: " << time_duration << "s" << std::endl << std::endl;

    else if (pulses != 0)
        std::cout << "Number of pulses transmitted: " << pulses << std::endl << std::endl;

    else if (total_num_samps != 0)
        std::cout << "Total samples sent: " << total_num_samps << std::endl << std::endl;

    return total_num_samps;
}

///*****************************************//
///           receive_to_file               //
///*****************************************//
void receive_to_file (
    uhd::usrp::multi_usrp::sptr usrp,
    uhd::rx_streamer::sptr& rx_stream,
    size_t samples_per_buffer,
    size_t total_num_samps,
    std::string& rx_file,
    double total_time,
    double pri,
    double pulse_width,
    size_t total_num_pulses
) {
    // setup the metadata flags
    uhd::rx_metadata_t rx_md;
    double timeout = pri; //seconds

    // read entire file into buffer a buffer
    std::vector<std::complex<short> > rx_buffs(samples_per_buffer);
    std::cout << "Allocated receive buffer with " << samples_per_buffer << " complex short samples" << std::endl;

    // open file to write to
    std::ofstream out_file;
    out_file.open(rx_file.c_str(), std::ofstream::binary);

    // setup streaming
    //uhd::stream_cmd_t stream_cmd = uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE;
    //uhd::stream_cmd_t stream_cmd = uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS;
    uhd::stream_cmd_t stream_cmd((total_num_samps == 0)?
        uhd::stream_cmd_t::STREAM_MODE_START_CONTINUOUS:
        uhd::stream_cmd_t::STREAM_MODE_NUM_SAMPS_AND_DONE
    );
    stream_cmd.num_samps = total_num_samps;
    stream_cmd.stream_now = true;
    stream_cmd.time_spec = uhd::time_spec_t(timeout);
    rx_stream->issue_stream_cmd(stream_cmd);

    // get start time
    boost::system_time start = boost::get_system_time();
    boost::system_time last_update = start;
    boost::posix_time::time_duration diff;

    //bool one_packet = true;
    size_t pulses = 0;
    double time_duration = 0.0;
    size_t num_acc_samps = 0;
    while (not stop_signal_called and (num_acc_samps < total_num_samps or total_num_samps == 0)) {
        boost::system_time now = boost::get_system_time(); // get current system time

        size_t samps_to_rx = samples_per_buffer;

        size_t samps_received;

        if ((time_duration > (double)total_time) and (total_time >= 0))	break;	//receive duration has been exceeded

        else if ((pulses >= total_num_pulses) and (pri > 0))	break;	//all requested pulses have been received

        if (((double)total_time != 0) and (time_duration <= (double)total_time)) {
            // request data from streamer and fill buffer
            size_t num_rx_samps = rx_stream->recv(
                &rx_buffs.front(), samps_to_rx, rx_md, timeout
            );

            samps_received = num_rx_samps;

            diff = now - last_update;
            time_duration += (double)diff.ticks() / (double)boost::posix_time::time_duration::ticks_per_second();

            last_update = now;

            std::cout << "Receive duration: " << time_duration << " [s]" << std::endl;
            //num_acc_samps += num_rx_samps; //update accumulated samples
        }

        if (pulses < total_num_pulses) {
            size_t num_rx_samps = rx_stream->recv(
                &rx_buffs.front(), samps_to_rx, rx_md, pri
            );

            samps_received = num_rx_samps;

            //double dead_time = pri - pulse_width;
            //boost::this_thread::sleep(boost::posix_time::seconds(dead_time));

            pulses++;
            std::cout << "Pulse " << pulses << " received" << std::endl;

            //if (pulses == total_num_pulses - 1)	one_packet = 1;
        }

        else {
            // request data from streamer and fill buffer
            size_t num_rx_samps = rx_stream->recv(
                &rx_buffs.front(), samps_to_rx, rx_md, timeout
            );

            samps_received = num_rx_samps;

            num_acc_samps += num_rx_samps; //update accumulated samples
            //std::cout << "Samples received: " << num_acc_samps << std::endl;
        }

        //handle the error code
        //if (rx_md.error_code == uhd::rx_metadata_t::ERROR_CODE_TIMEOUT) break;
        if (rx_md.error_code != uhd::rx_metadata_t::ERROR_CODE_NONE){
            throw std::runtime_error(str(boost::format("Receiver error %s") % rx_md.strerror()));
        }

        timeout = 0.1;	//reduce time between successive streams

        // write buffer to file
        out_file.write((const char*)&rx_buffs.front(), 2 * samps_received * sizeof(short)); //complex values [i,j]
    }

    // Shut down receiver
    stream_cmd.stream_mode = uhd::stream_cmd_t::STREAM_MODE_STOP_CONTINUOUS;
    rx_stream->issue_stream_cmd(stream_cmd);

    out_file.close();	//close file

    std::cout << std::endl << "Finished streaming!" << std::endl;

    if ((double)total_time != 0)
        std::cout << "Total receive time: " << time_duration << "s" << std::endl << std::endl;

    else if (total_num_samps != 0)
        std::cout << "Total samples received: " << total_num_samps << std::endl << std::endl;

}

///*****************************************//
///             UHD_SAFE_MAIN               //
///*****************************************//
int UHD_SAFE_MAIN(int argc, char* argv[]) {
    uhd::set_thread_priority_safe();

    // transmit and receive variables to be set by po
    std::string args, ref, channel, tx_file, rx_file;
    size_t total_num_samps, spb, num_pulses;
    double total_time, prf;
    double tx_rate, tx_freq, tx_gain, tx_bw;    //transmit variables
    double rx_rate, rx_freq, rx_gain, rx_bw;    //receive variables

    // setup the program options
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "help message")
        ("args", po::value<std::string>(&args)->default_value("addr=192.168.10.2"), "uhd transmit device address args")
        ("tx-file", po::value<std::string>(&tx_file)->default_value("../input/samples_tx.bin"), "name of the file to read samples from")
        ("rx-file", po::value<std::string>(&rx_file)->default_value("../output/samples_rx.bin"), "name of the file to write samples to")

        ("nsamps", po::value<size_t>(&total_num_samps)->default_value(0), "total number of samples to receive")
        ("duration", po::value<double>(&total_time)->default_value(0), "total number of seconds to receive")
        ("spb", po::value<size_t>(&spb)->default_value(1), "samples per buffer, 1 as default")
        ("npulses", po::value<size_t>(&num_pulses)->default_value(0), "number of pulses to transmit")
        ("prf", po::value<double>(&prf)->default_value(0), "pulse repetition frequency for pulsed operation")

        ("tx-rate", po::value<double>(&tx_rate)->default_value(1e6), "rate of transmit outgoing samples [MHz]")
        ("rx-rate", po::value<double>(&rx_rate)->default_value(1e6), "rate of receive incoming samples [MHz]")
        ("tx-freq", po::value<double>(&tx_freq)->default_value(900e6), "transmit RF center frequency in Hz")
        ("rx-freq", po::value<double>(&rx_freq)->default_value(900e6), "receive RF center frequency in Hz")
        ("tx-gain", po::value<double>(&tx_gain)->default_value(0), "gain for the transmit RF chain")
        ("rx-gain", po::value<double>(&rx_gain)->default_value(0), "gain for the receive RF chain")

        ("tx-bw", po::value<double>(&tx_bw)->default_value(0), "analog transmit filter bandwidth in Hz")
        ("rx-bw", po::value<double>(&rx_bw)->default_value(0), "analog receive filter bandwidth in Hz")

        ("ref", po::value<std::string>(&ref)->default_value("internal"), "clock reference (internal, external, mimo)")
        ("channel", po::value<std::string>(&channel)->default_value("0"), "which channel to use")
        ("tx-int-n", "tune USRP TX with integer-N tuning")
        ("rx-int-n", "tune USRP RX with integer-N tuning")
        ;
    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    // print the help message
    if (vm.count("help")) {
        std::cout << boost::format("\nUSRP N210 Loopback to File %s") % desc << std::endl << std::endl;
        return ~0;
    }

    // create a usrp device
    std::cout << std::endl;
    std::cout << boost::format("Creating the transmit/receive usrp device with: %s...") % args << std::endl;
    uhd::usrp::multi_usrp::sptr usrp = uhd::usrp::multi_usrp::make(args);

    // setting device timestamp
    std::cout << "Setting device timestamp..." << std::endl;
    usrp->set_clock_source(ref);

    // always select the subdevice first, the channel mapping affects the other settings
    usrp->set_tx_subdev_spec(uhd::usrp::subdev_spec_t("A:0"), size_t(0));
    usrp->set_rx_subdev_spec(uhd::usrp::subdev_spec_t("A:0"), size_t(0));

    std::cout << boost::format("Using Device: %s") % usrp->get_pp_string() << std::endl << std::endl;

    // set the transmit sample rate
    std::cout << boost::format("Setting TX Rate: %f Msps...") % (tx_rate / 1e6) << std::endl;
    usrp->set_tx_rate(tx_rate);
    std::cout << boost::format("Actual TX Rate: %f Msps...") % (usrp->get_tx_rate() / 1e6) << std::endl << std::endl;

    // set the transmit center frequency
    std::cout << boost::format("Setting TX Freq: %f MHz...") % (tx_freq / 1e6) << std::endl;
    uhd::tune_request_t tx_tune_request(tx_freq);
    if (vm.count("tx-int-n")) tx_tune_request.args = uhd::device_addr_t("mode_n=integer");
    usrp->set_tx_freq(tx_tune_request);
    std::cout << boost::format("Actual TX Freq: %f MHz...") % (usrp->get_tx_freq() / 1e6) << std::endl << std::endl;

    // set the transmit gain [dB]
    std::cout << boost::format("Setting TX Gain: %f dB...") % tx_gain << std::endl;
    usrp->set_tx_gain(tx_gain);
    std::cout << boost::format("Actual TX Gain: %f dB...") % usrp->get_tx_gain() << std::endl << std::endl;

    // set the transmit bandwidth
    if (tx_bw <= 0)    tx_bw = tx_rate;
    std::cout << boost::format("Setting TX Bandwidth: %f MHz...") % (tx_bw / 1e6) << std::endl;
    usrp->set_tx_bandwidth(tx_bw);
    std::cout << boost::format("Actual TX Bandwidth: %f MHz...") % (usrp->get_tx_bandwidth() / 1e6) << std::endl << std::endl;

    // set the receive sample rate
    std::cout << boost::format("Setting RX Rate: %f Msps...") % (rx_rate / 1e6) << std::endl;
    usrp->set_rx_rate(rx_rate);
    std::cout << boost::format("Actual RX Rate: %f Msps...") % (usrp->get_rx_rate() / 1e6) << std::endl << std::endl;

    // set the receive center frequency
    std::cout << boost::format("Setting RX Freq: %f MHz...") % (rx_freq / 1e6) << std::endl;
    uhd::tune_request_t rx_tune_request(rx_freq);
    if (vm.count("rx-int-n")) rx_tune_request.args = uhd::device_addr_t("mode_n=integer");
    usrp->set_rx_freq(rx_tune_request);
    std::cout << boost::format("Actual RX Freq: %f MHz...") % (usrp->get_rx_freq() / 1e6) << std::endl << std::endl;

    // set the receive gain [dB]
    std::cout << boost::format("Setting RX Gain: %f dB...") % rx_gain << std::endl;
    usrp->set_rx_gain(rx_gain);
    std::cout << boost::format("Actual RX Gain: %f dB...") % usrp->get_rx_gain() << std::endl << std::endl;

    // set the receive bandwidth
    if (rx_bw <= 0)    rx_bw = rx_rate;
    std::cout << boost::format("Setting RX Bandwidth: %f MHz...") % (rx_bw / 1e6) << std::endl;
    usrp->set_rx_bandwidth(rx_bw);
    std::cout << boost::format("Actual RX Bandwidth: %f MHz...") % (usrp->get_rx_bandwidth() / 1e6) << std::endl << std::endl;

    // set the antenna
    usrp->set_tx_antenna("TX/RX", 0);
    usrp->set_rx_antenna("RX2", 0);

    boost::this_thread::sleep(boost::posix_time::seconds(1)); //allow for some setup time

    if (total_num_samps == 0){
        std::signal(SIGINT, &sig_int_handler);
        std::cout << "Press Ctrl + C to stop streaming..." << std::endl << std::endl;
    }

    double pri = 0;	//pulse repetition interval
    if (prf != 0){
        pri = 1.0 / prf;
    }

    double pulse_width = 1 / tx_bw;	//using pulse compression ratio of 1

    //reset usrp time to prepare for transmit/receive
    std::cout << boost::format("Setting device timestamp to 0...") << std::endl << std::endl;
    usrp->set_time_now(uhd::time_spec_t(0.0));

    // set stream arguments
    std::vector<size_t> channel_nums;
    channel_nums.push_back(boost::lexical_cast<size_t>(channel));
    uhd::stream_args_t stream_args_tx("sc16");
    stream_args_tx.channels = channel_nums;

    // create a transmit streamer
    uhd::tx_streamer::sptr tx_stream = usrp->get_tx_stream(stream_args_tx);

    // call transmit_from_file
    const int tx_samples_per_buffer = tx_stream->get_max_num_samps() * spb;
    size_t num_file_samps = transmit_from_file(usrp, tx_stream, tx_samples_per_buffer, tx_file, total_time, pri, pulse_width, num_pulses);

    //boost::this_thread::sleep(boost::posix_time::seconds(1.5)); //allow for some sleep time

    // set stream arguments
    uhd::stream_args_t stream_args_rx("sc16");
    stream_args_rx.channels = channel_nums;

    // create a receive streamer
    uhd::rx_streamer::sptr rx_stream = usrp->get_rx_stream(stream_args_rx);

    // call receive_to_file
    const int rx_samples_per_buffer = rx_stream->get_max_num_samps() * spb;
    if ((total_num_samps == 0) && (num_file_samps != 0))
        total_num_samps = num_file_samps;

    receive_to_file(usrp, rx_stream, rx_samples_per_buffer, total_num_samps, rx_file, total_time, pri, pulse_width, num_pulses);

    // finished
    return EXIT_SUCCESS;
}
