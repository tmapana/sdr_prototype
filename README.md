# SDR Prototype

Software-defined radar prototype developed on Ettus USRP N210

This repository is part of a software-defined radar (SDR) prototype developed for the completion of an Electrical & Computer Engineering final year project.

**CMakeLists.txt** contains the environment setup instructions needed to build project files.

### MATLAB Files
There are two files with the extention **.m** which can be executed in a MATLAB environment no a Windows or a Linux operating system

### C++ Files
The **.cpp** file contains the driver for this project.

### Installation
The project can be installed from a Linux terminal as follows:
```bash
mkdir build
cd build
cmake ../
```

### Compilation and Execution
To compile the project in the **build** directory and to run the executable, follow these steps:
```bash
make
./usrp_n210_tx_rx [program_options]
```
