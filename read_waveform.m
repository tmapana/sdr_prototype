clc; clear; close all;

% open file
fprintf('Reading BIN file..\n');
file_name = 'output\samples_rx.bin';
fid = fopen(file_name, 'r', 'ieee-le');

% extract raw data
fseek(fid, 0, 'bof'); % fileID, Offset in Bytes, Start point
raw_data = fread(fid, inf, 'short', 0, 'ieee-le');
fclose(fid);

% Read the data
data = complex(raw_data(1:2:end), raw_data(2:2:end));
data_FFT = fftshift(fft(data(1:end,1:end)));

% plot waveform
figure();
plot(real(raw_data));

fprintf('Complete.\n');