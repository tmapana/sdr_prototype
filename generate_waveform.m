clc; clear; close all;

fprintf('Generating waveform..\n');

% generate simple sine wave
t = (0:0.1:10)';
A = 1;  % amplitude
fc = 10e6; % carrier frequency 10MHz
y = A*sin(fc*t); % sine wave

% open file to write to
file_name = 'input\samples_tx.bin';
offset = 0;
fid = fopen(file_name, 'w', 'ieee-le');

% write to file
fseek(fid, offset, 'bof');
fwrite(fid, y, 'short', 'ieee-le');
fclose(fid);

% plot wave
figure();
plot(t, y);

fprintf('Complete.\n');